import itertools
import os
import shutil
import time

import matplotlib.pyplot as plt
import numpy as np
from keras.callbacks import EarlyStopping
from keras.layers import Dense
from keras.layers import MaxPooling2D, SeparableConv2D, BatchNormalization
from keras.layers.core import Flatten, Dropout, regularizers
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import confusion_matrix

from spectrograms import IMG_SIZE


def count_samples(directory):
    cpt = 0
    for root, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith(".png"):
                cpt += 1
    return cpt


class NectarModel:
    def __init__(self, image_size, batch_size=128, train_epochs=50):
        self.model = None
        self.confusion_matrix = []
        self.normalized_confusion_matrix = []
        self.confusion_matrix_png = None
        self.val_accuracy = 0
        self.test_accuracy = 0
        self.image_size = image_size
        self.input_shape = (IMG_SIZE, IMG_SIZE, 1)
        self.batch_size = batch_size
        self.class_mode = 'binary'
        self.color_mode = 'grayscale'
        self.train_generator = None
        self.validation_generator = None
        self.test_generator = None
        self.data_set_report = ""
        self.train_epochs = train_epochs
        self.class_labels = []

    def compile(self, batch_normalize=True, conv_coeffs=[32, 64, 64, 128, 128, 256], dense_coeff=32, k2=0.01, XXX=0.2):  # arg in INIT
        model = Sequential()
        model.add(SeparableConv2D(32, (3, 3), activation='relu', input_shape=self.input_shape, name="inputLayer"))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(SeparableConv2D(32, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2)))
        model.add(SeparableConv2D(64, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(SeparableConv2D(64, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2)))
        model.add(SeparableConv2D(128, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(SeparableConv2D(128, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2)))
        model.add(SeparableConv2D(256, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(SeparableConv2D(256, (3, 3), activation='relu'))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(MaxPooling2D((2, 2)))

        model.add(Flatten())
        model.add(Dense(32, activation='relu', kernel_regularizer=regularizers.l2(0.05)))
        if batch_normalize:
            model.add(BatchNormalization())
        model.add(Dropout(0.5))
        model.add(Dense(1, activation='sigmoid', name="inferenceLayer"))

        model.compile(loss='binary_crossentropy',
                      optimizer='adam',
                      metrics=['accuracy'])

        print(model.summary())
        self.model = model

    def get_generators(self, train_dir, validation_dir, test_dir):
        train_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
        validation_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
        test_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
        self.train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(self.image_size, self.image_size),
            shuffle=True,
            batch_size=self.batch_size,
            class_mode=self.class_mode,
            color_mode=self.color_mode
        )
        self.validation_generator = validation_datagen.flow_from_directory(
            validation_dir,
            target_size=(self.image_size, self.image_size),
            batch_size=self.batch_size,
            shuffle=True,
            class_mode=self.class_mode,
            color_mode=self.color_mode
        )
        self.test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(self.image_size, self.image_size),
            batch_size=self.batch_size,
            shuffle=False,
            class_mode=self.class_mode,
            color_mode=self.color_mode
        )
        # print(validation_generator.class_indices)

    def train(self):
        nb_train_samples = len(self.train_generator.filenames)
        nb_validation_samples = len(self.validation_generator.filenames)

        # callback
        print("Training model...")
        early_stopping = EarlyStopping(monitor='val_loss', patience=5, mode='auto')
        # the way the data set is generated, gives about 5x more bad encodes than good ones.
        # setting weights to counter the imbalance.
        class_weights = {0: 1., 1: 6}
        history = self.model.fit_generator(self.train_generator,
                                           steps_per_epoch=(nb_train_samples // self.batch_size),
                                           epochs=self.train_epochs,
                                           validation_data=self.validation_generator,
                                           callbacks=[early_stopping],
                                           validation_steps=(nb_validation_samples // self.batch_size),
                                           class_weight=class_weights
                                           )
        self.val_accuracy = history.history['val_acc'][-1]

    def evaluate(self):
        self.test_generator.reset()
        loss, accuracy = self.model.evaluate_generator(self.test_generator, steps=None, max_queue_size=10,
                                                       workers=12, use_multiprocessing=True)
        self.test_accuracy = accuracy
        self.generate_confusion_matrix()

    def generate_confusion_matrix(self):
        self.test_generator.reset()
        # get the predictions
        predictions = self.model.predict_generator(self.test_generator, steps=None, workers=12, use_multiprocessing=True,
                                                   verbose=0)
        predictions = predictions[:, 0]
        # get all of the original categories
        self.test_generator.reset()
        original_categories = []
        while len(original_categories) < len(predictions):
            test_imgs, batch_categories = next(self.test_generator)
            original_categories.extend(batch_categories)
        original_categories = original_categories[:len(predictions)]

        # calculate the confusion matrix
        self.confusion_matrix = confusion_matrix(original_categories, np.round(predictions))
        self.normalized_confusion_matrix = self.confusion_matrix.astype('float') / self.confusion_matrix.sum(axis=1)[:, np.newaxis]
        self.normalized_confusion_matrix = 100 * self.normalized_confusion_matrix
        print("Normalized confusion matrix")
        print(self.normalized_confusion_matrix)
        print('Confusion matrix, without normalization')
        print(self.confusion_matrix)
        class_labels = {v: k for k, v in self.test_generator.class_indices.items()}
        self.class_labels = [class_labels[0], class_labels[1]]

    def save_confusion_matrix(self, filename, title='Confusion matrix -- for spectrogram slices, not audio tracks', cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        """
        plt.imshow(self.confusion_matrix, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(self.class_labels))
        plt.xticks(tick_marks, self.class_labels, rotation=45)
        plt.yticks(tick_marks, self.class_labels)

        thresh = self.confusion_matrix.max() / 2.
        for i, j in itertools.product(range(self.confusion_matrix.shape[0]), range(self.confusion_matrix.shape[1])):
            plt.text(j, i, "%d (%.2f%%)" % (self.confusion_matrix[i, j], self.normalized_confusion_matrix[i, j]),
                     horizontalalignment="center",
                     color="white" if self.confusion_matrix[i, j] > thresh else "black")
        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.savefig(filename, bbox_inches='tight')

    def save(self, models_dir):
        """
        save the model and its metadata.
        :param models_dir: target directory
        """
        model_id = "%s_nectar_%sval_%stest" % (int(time.time()), int(100 * self.val_accuracy), int(100 * self.test_accuracy))
        full_model_dir = os.path.join(models_dir, model_id)
        model_name = os.path.join(full_model_dir, "model")
        # saving model
        if not os.path.exists(full_model_dir):
            os.makedirs(full_model_dir)

        self.model.save(model_name + '.h5')
        with open(model_name + '.yaml', 'w') as f:
            f.write(self.model.to_yaml())
        with open(model_name + '.txt', 'w') as f:
            self.model.summary(print_fn=lambda x: f.write(x + '\n'))
            f.write("Results:\n")
            f.write("Data set: " + self.data_set_report + "\n")
            # TODO class indices?
            f.write("Validation accuracy: %.3f%%, Test accuracy: %.3f%%\n" % (100 * self.val_accuracy, 100 * self.test_accuracy))
            f.write("Confusion matrix:\n")
            f.write(np.array_str(np.asarray(self.confusion_matrix), precision=3, suppress_small=True))
            f.write("Normalized confusion matrix:\n")
            f.write(np.array_str(np.asarray(self.normalized_confusion_matrix), precision=3, suppress_small=True))
        # TODO cleaner
        if os.path.exists("data_set_summary.txt"):
            shutil.copyfile("data_set_summary.txt", model_name + "_data_set.txt")
        self.save_confusion_matrix(model_name + "_confusion_matrix.png", title='Confusion Matrix')

    def load_samples(self, specs_dir):
        training_dir = os.path.join(specs_dir, "training")
        validation_dir = os.path.join(specs_dir, "validation")
        test_dir = os.path.join(specs_dir, "test")

        num_training_samples = count_samples(training_dir)
        num_validation_samples = count_samples(validation_dir)
        num_test_samples = count_samples(test_dir)
        total = num_training_samples + num_validation_samples + num_test_samples
        if total == 0:
            print("No samples found")
            exit(-1)
        report = "%s samples found: %s (%.2f%%) for training,  %s (%.2f%%) for validation, %s (%.2f%%) for testing."
        self.data_set_report = report % (total,
                                         num_training_samples,
                                         (100.0 * num_training_samples) / total,
                                         num_validation_samples,
                                         (100.0 * num_validation_samples) / total,
                                         num_test_samples,
                                         (100.0 * num_test_samples) / total)
        print(self.data_set_report)
        self.get_generators(training_dir, validation_dir, test_dir)


def train_from_scratch(specs_dir, models_dir, batch_size):
    # make model
    m = NectarModel(IMG_SIZE, batch_size=batch_size, train_epochs=50)
    m.compile()
    m.load_samples(specs_dir)
    m.train()
    m.evaluate()
    m.save(models_dir)
    return m
