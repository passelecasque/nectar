import csv
import glob
import json
import os
from concurrent.futures import ProcessPoolExecutor, as_completed

from audio_file import SourceFile


class SourceDir:
    def __init__(self, path, tracker=None):
        self.path = path
        self.files = []
        if tracker:
            self.metadata_json = os.path.join(self.path, "TrackerMetadata", tracker + "_release.json")
        self.metadata = {}

    def __str__(self):
        return "{artists}: {release} [{tid}] | {tracks} tracks | {tags}".format(artists=', '.join(self.metadata["artists"]),
                                                                                release=self.metadata["title"],
                                                                                tid=self.metadata["id"],
                                                                                tracks=self.metadata["tracks"],
                                                                                tags=", ".join(self.metadata["tags"]))

    def has_music(self):
        for root, dirs, files in os.walk(self.path):
            for f in files:
                if os.path.splitext(f)[1].lower() == ".flac":
                    return True
        return False

    def get_files(self):
        self.files = [SourceFile(el) for el in sorted(glob.glob(os.path.join(glob.escape(self.path), '**', '*.flac'), recursive=True))]
        self.metadata["tracks"] = len(self.files)

    def is_candidate(self):
        return os.path.exists(self.metadata_json) and self.has_music()

    def get_metadata(self):
        with open(self.metadata_json, "r") as f:
            metadata = json.load(f)
            self.metadata["id"] = metadata['torrent']['id']
            self.metadata["tags"] = metadata['group']['tags']
            self.metadata["source"] = metadata['torrent']['media']
            # if CD, make sure the log score is 100 TODO only for the selected
            # if tinfo["source"] == "CD":
            #    assert metadata['torrent']['logScore'] == 100
            self.metadata["title"] = metadata['group']['name']
            self.metadata["artists"] = []
            for a in metadata['group']['musicInfo']['artists']:
                self.metadata["artists"].append(a["name"])

    def is_selected(self, id_list):
        if self.is_candidate():
            self.get_metadata()
            return self.metadata["id"] in id_list
        return False


class DataSet:
    def __init__(self, source_dir, audio_dir, full_specs_dir, specs_dir, tracker=None, csv_file=None):
        assert os.path.exists(source_dir)
        self.root = source_dir
        self.audio_dir = audio_dir
        self.full_specs_dir = full_specs_dir
        self.specs_dir = specs_dir
        self.source_dirs = []

        self.varroa = csv_file is not None and tracker is not None
        if self.varroa:
            self.tracker = tracker
            self.ids = []
            self.load_csv(csv_file)
        else:
            # the source dir is the root
            s = SourceDir(self.root)
            s.get_files()
            self.source_dirs.append(s)

    def load_csv(self, csv_file):
        assert os.path.exists(csv_file)

        with open(csv_file, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for (i, row) in enumerate(reader):
                try:
                    self.ids.append(int(row[0]))
                except ValueError:
                    print("invalid row on line %d: %s" % (i, row[0]))

    def scan_for_source_files(self):
        cpt = 0
        if self.varroa:
            total_number = len(self.ids)
            for d in sorted(os.listdir(self.root)):
                sd = SourceDir(os.path.join(self.root, d), self.tracker)
                if sd.is_selected(self.ids):
                    sd.get_files()
                    cpt += 1
                    print("(%d/%d) Found source: %s" % (cpt, total_number, d))
                    self.source_dirs.append(sd)
                    self.ids.remove(sd.metadata["id"])
                if len(self.ids) == 0:
                    break

            # list the ids that were not found
            if len(self.ids) != 0:
                print("These torrent ids were not found: " + ", ".join(str(x) for x in self.ids))
        else:
            sd = SourceDir(self.root)
            sd.get_files()

    def summary(self):
        if not self.varroa:
            return

        num_releases = 0
        num_tracks = 0
        num_tracks_by_tag = {}
        num_tracks_by_source = {}
        for sd in self.source_dirs:
            num_releases += 1
            num_tracks += sd.metadata["tracks"]
            for t in sd.metadata["tags"]:
                if t in num_tracks_by_tag.keys():
                    num_tracks_by_tag[t] += sd.metadata["tracks"]
                else:
                    num_tracks_by_tag[t] = sd.metadata["tracks"]
            if sd.metadata["source"] in num_tracks_by_source.keys():
                num_tracks_by_source[sd.metadata["source"]] += sd.metadata["tracks"]
            else:
                num_tracks_by_source[sd.metadata["source"]] = sd.metadata["tracks"]

        summary = "Total number of source flacs: %d (in %d releases)\n" % (num_tracks, num_releases)

        summary += "By tag (tracks can have multiple tags):\n"
        for t in sorted(num_tracks_by_tag.keys()):
            summary += " - %s: %d (%.2f%%)\n" % (t, num_tracks_by_tag[t], 100 * num_tracks_by_tag[t] / num_tracks)

        summary += "By source:\n"
        for t in sorted(num_tracks_by_source.keys()):
            summary += " - %s: %d (%.2f%%)\n" % (t, num_tracks_by_source[t], 100 * num_tracks_by_source[t] / num_tracks)

        items = []
        for sd in self.source_dirs:
            items.append(" - %s\n" % str(sd))
        summary += "Contents:\n"
        for i in items:
            summary += i

        print(summary)
        with open("data_set_summary.txt", "w") as f:
            f.write(summary)
        return summary

    def prepare(self):
        print("Preparing...")
        # generate all transcodes
        all_source_files = []
        for sd in self.source_dirs:
            all_source_files.extend(sd.files)

        cpt = 0
        samples = 0
        with ProcessPoolExecutor() as executor:
            futures = {executor.submit(f.prepare, self.audio_dir, self.full_specs_dir, self.specs_dir, 0.15, 0.15): f for f in all_source_files}
            for future in as_completed(futures):
                cpt += 1
                samples += future.result()
                print("%.2f%% : Finished generating all spectrograms for %s." % ((100.0 * cpt) / len(all_source_files), futures[future].filename))
        print("Finished preparing the data set. Created %d samples." % samples)
