#!/usr/bin/env python3

"""nectar.

Commands:
  init:
                prepare the data set by using all FLACs in the source directory
  init-varroa:
                prepare the data set using only the releases from TRACKER_NAME
                whitelisted in IDLIST_FILE, if they are found in the source
                directory. This assumes the releases were snatched with
                varroa, with metadata retrieval turned on.
  train:
                train the model on the prepared data set.
  analyze:
                analyze an audio file or a whole directory containing audio
                files.

Usage:
  nectar.py init
  nectar.py init-varroa TRACKER_NAME IDLIST_FILE [--identify-only]
  nectar.py train
  nectar.py analyze AUDIO_FILE_OR_FOLDER [--dev-models]
  nectar.py (-h | --help)
  nectar.py --version

Options:
  -h --help         Show this screen.
  --version         Show version.
  --identify-only   Identify the folders to be used for the data set
                    and generate the summary.
  --dev-models      Loop over all dev models previously trained and
                    saved.

"""
import os
import glob
from shutil import which

from colorama import init, Fore, Style
from docopt import docopt

from data_set import DataSet, SourceDir
from audio_file import SourceFile, analyze
from spectrograms import IMG_SIZE
from model import train_from_scratch

init(autoreset=True)

# preliminary checks
try:
    assert which("sox") is not None
    assert which("ffmpeg") is not None
except AssertionError as e:
    print("Required commands cannot be found. Please make sure sox & ffmpeg are installed.")
    exit(-1)

try:
    from config import (default_source_dir,
                        default_data_dir,
                        default_full_specs_dir,
                        default_specs_dir,
                        default_batch_size,
                        )
except ImportError:
    default_source_dir = default_data_dir = \
        default_full_specs_dir = default_specs_dir = None
    default_batch_size = 64


def print_red(s):
    print(Fore.RED + Style.BRIGHT + s)


def print_green(s):
    print(Fore.GREEN + Style.BRIGHT + s)


def main():
    print(Fore.BLUE + Style.BRIGHT + "\n★★★ nectar ★★★\n")

    args = docopt(__doc__)
    if args['init-varroa']:
        print_red("This assumes that your source dir contains releases as downloaded by varroa (with tracker metadata)")
        print_red("The data set is defined by a list of torrent ids in " + args['IDLIST_FILE'])
        print_red("nectar will try to find the corresponding releases in " + default_source_dir + " using the JSONs saved by varroa.")
        print_red("It will then display a quick summary of what was selected and found, using the tracker tags.")
        print("If not, it will accomplish nothing. It will try, but miserably fail.")
        d = DataSet(default_source_dir, default_data_dir, default_full_specs_dir, default_specs_dir, tracker=args['TRACKER_NAME'], csv_file=args['IDLIST_FILE'])
        # launch scan
        d.scan_for_source_files()
        # generate summary
        d.summary()
        if not args['--identify-only']:
            d.prepare()

    elif args['init']:
        d = DataSet(default_source_dir, default_data_dir, default_full_specs_dir, default_specs_dir)
        d.prepare()

    elif args['train']:
        m = train_from_scratch(default_specs_dir, "models", batch_size=default_batch_size)
        print_green("Model trained from data set: %.2f%% validation accuracy, %.2f%% test accuracy." % (100 * m.val_accuracy,
                                                                                                        100 * m.test_accuracy))

    elif args['analyze']:
        candidate = args['AUDIO_FILE_OR_FOLDER']
        if not os.path.exists(candidate):
            print_red("File " + candidate + " does not exist")
            exit(-1)

        all_files = []
        if os.path.isdir(candidate):
            sd = SourceDir(candidate)
            sd.get_files()
            all_files = sd.files
        elif os.path.isfile(candidate):
            all_files.append(SourceFile(candidate))

        if args['--dev-models']:
            models = sorted(glob.glob(os.path.join(os.path.dirname(__file__), 'models','**/*.h5'), recursive=True))
        else:
            models = [os.path.join(os.path.dirname(__file__), 'model.h5')]
        analyze(all_files, models, IMG_SIZE, batch_size=default_batch_size)

    else:
        print_red("Incorrect output, see usage.")


if __name__ == "__main__":
    main()
