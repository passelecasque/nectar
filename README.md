# nectar

Tired of squinting at spectrals to rid the world of bad transcodes? 

Well good news everyone! 
We live in the future, and we can have a machine do that for us!

Harnessing the magical and unfathomable powers of deep learning, nectar is a new tool that can give you a probability that a given file is, in fact, an awful transcode.

It has been trained to detect legit 16-bit 44.1kHz FLACs, and to discriminate against 320/256/V0/V2/AAC->FLAC transcodes.

This is `nectar`. It stands for:
**n**ectar
**e**xposes
**c**riminal
**t**ranscodes;
**a**ll
**r**ejoice.

## Why

I wanted to play around with deep learning. This seemed like a semi-useful application.

## Requirements

Getting this running is not particularly user-friendly.

You need to have `python-tensorflow`, `tensorflow`, `python-keras`, `python-scikit-learn`, `python-colorama`, `numpy`, `PIL` and `sox` installed. If you want to train the model from your own dataset, you need `ffmpeg` as well. 

If you are looking to train the model, copy `config.py.example` to `config.py` and insert your 
values.

## Usage:

    ★★★ nectar ★★★
    
    nectar.
    
    Commands:
      init:
                    prepare the data set by using all FLACs in the source directory
      init-varroa:
                    prepare the data set using only the releases from TRACKER_NAME
                    whitelisted in IDLIST_FILE, if they are found in the source
                    directory. This assumes the releases were snatched with
                    varroa, with metadata retrieval turned on.
      train:
                    train the model on the prepared data set.
      analyze:
                    analyze an audio file or a whole directory containing audio
                    files.
    
    Usage:
      nectar.py init
      nectar.py init-varroa TRACKER_NAME IDLIST_FILE [--identify-only]
      nectar.py train
      nectar.py analyze AUDIO_FILE_OR_FOLDER [--dev-models]
      nectar.py (-h | --help)
      nectar.py --version
    
    Options:
      -h --help         Show this screen.
      --version         Show version.
      --identify-only   Identify the folders to be used for the data set
                        and generate the summary.
      --dev-models      Loop over all dev models previously trained and
                        saved.

# How does it work?

`nectar` uses a convnet trained on the high-frequency part of spectrograms.

Here's the general idea of how the model was trained:

- Start with flac files from many genres.
- Randomly select 50% of them to be used as they are.

*We now have a "good encodes" set of files.*

- Use the other 50% and transcode them to V0, V2, 320 CBR & 256 CBR mp3s, and 256k AAC.
- Transcode all of them back to flac and remove the intermediary files.

*We now have a "bad transcodes" set of files.* We may have felt a little dirty for this part, but it's for the greater good.

For each file of each set:
- generate its spectrogram with 128px of width corresponding to 5 seconds of audio time, and 256px of height.
- slice the spectrogram into 5s slices, using only the top half (representing high frequencies, the only useful part for detecting bad transcodes).

*We now have two sets of 128x128 slices of spectrograms, each representing 5s of the high frequencies of good encodes or bad transcodes.*

`nectar` removes slices identified as silence (blank slices) from the set.

`nectar` splits the sets into training/validation/test sets, and uses them to train a convolutional neural network to categorize the spectrogram slices.

Once the deep learning magic has happened, nectar can load the trained model and run it over the spectrogram slices of any given file, and give a probability of being either a good encode or a bad transcode.

# Disclaimers

## I have no idea what I'm doing

If I've done something obviously wrong to a deep learning-trained eye, please speak up!

## Help me train it better!

A deep learning model is only reliable if it has been trained on a representative data set. If you see nectar make a classification error, please tell me which release triggered it, so that I can incorporate it in the training set and re-train the model.

## Original inspiration
 
http://mattmurray.net/building-a-music-recommender-with-deep-learning/
