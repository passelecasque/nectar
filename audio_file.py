import hashlib
import os
import shutil
import tempfile
from enum import Enum
from subprocess import PIPE, check_output, check_call

import numpy as np
from colorama import Fore, Style
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator

from spectrograms import Looks, Spectrogram


class Q(Enum):
    FLAC = "original"
    MP3_320 = "cbr320"
    MP3_256 = "cbr256"
    MP3_v0 = "v0"
    MP3_v2 = "v2"
    AAC_V5 = "aac"


class Subset(Enum):
    TRAINING = "training"
    VALIDATION = "validation"
    TEST = "test"
    PREDICT = "predict"


def to_mp3_cbr(file, dest_file, rate):
    check_call(["ffmpeg", "-i", file, "-c:a", "libmp3lame", "-b:a", "%dk" % rate, dest_file], stdin=PIPE, stdout=PIPE, stderr=PIPE)


def to_mp3_vbr(file, dest_file, quality):
    check_call(["ffmpeg", "-i", file, "-c:a", "libmp3lame", "-q:a", "%d" % quality, dest_file], stdin=PIPE, stdout=PIPE, stderr=PIPE)


def to_aac(file, dest_file):
    check_call(["ffmpeg", "-i", file, "-c:a", "aac", "-b:a", "256k", dest_file], stdin=PIPE, stdout=PIPE, stderr=PIPE)


def to_flac(file, dest_file):
    check_call(["ffmpeg", "-i", file, "-c:a", "flac", "-compression_level", "12", dest_file], stdin=PIPE, stdout=PIPE, stderr=PIPE)


def has_right_sampling_rate(audio_file):
    """
    checks if the file has a 44.1kHz sampling rate
    :return: bool
    """
    return check_output(["sox", "--i", "-r", audio_file]).strip() == b"44100"


class AudioFile:
    """
    AudioFile represents a part of the audio data set, either a copy of the original flac or an awful transcode.
    It will have its spectrogram generated and cut into the slices that will be fed to the model.
    """
    def __init__(self, source, hash, set, quality, good_encode, audio_dir, predict=False):
        self.source = source
        if predict:
            self.filename = source
        self.hash = hash
        self.spectrogram = None
        self.good_encode = good_encode
        self.set = set
        self.quality = quality
        self.audio_dir = audio_dir
        self.possible_dest_filenames = {
            Subset.TRAINING: os.path.join(self.audio_dir, str(Subset.TRAINING.value), str(self.good_encode.value), self.hash + "_" + str(self.quality.value) + ".flac"),
            Subset.VALIDATION: os.path.join(self.audio_dir, str(Subset.VALIDATION.value), str(self.good_encode.value), self.hash + "_" + str(self.quality.value) + ".flac"),
            Subset.TEST: os.path.join(self.audio_dir, str(Subset.TEST.value), str(self.good_encode.value), self.hash + "_" + str(self.quality.value) + ".flac"),
            Subset.PREDICT: source,
        }
        self.filename = self.possible_dest_filenames[set]
        self.original_copy = os.path.join(self.audio_dir, str(self.set.value), str(Looks.GOOD.value), self.hash + "_" + str(Q.FLAC.value) + ".flac")

    def transcode(self):
        """
        An AudioFile object is created from a SourceFile and quality settings. The actual file needs to be created from
        this SourceFile, in the correct subset.
        :param audio_dir: directory of all input audio files
        :param set: subset the destination files belongs to
        :return: True if a file was created
        """
        # if running 'init' several times, the file might end up in random subsets.
        # checking all possibilities, skipping if any exist.

        for k, v in self.possible_dest_filenames.items():
            if os.path.exists(v):
                return False
        # if we get to here, we know the file has not been copied/transcoded yet.

        temp_filename = self.filename + ".temp.mp3"

        if not has_right_sampling_rate(self.source):
            print("Not a 44.1Khz audio file! ~ " + os.path.basename(self.source))
            return False
        # try to make the parent folders, if they exists, it's ok.
        os.makedirs(os.path.dirname(self.filename), exist_ok=True)

        if self.good_encode == Looks.GOOD:
            shutil.copyfile(self.source, self.original_copy)
            return True
        else:
            if self.quality == Q.MP3_320:
                to_mp3_cbr(self.source, temp_filename, 320)
            elif self.quality == Q.MP3_256:
                to_mp3_cbr(self.source, temp_filename, 256)
            elif self.quality == Q.MP3_v0:
                to_mp3_vbr(self.source, temp_filename, 0)
            elif self.quality == Q.MP3_v2:
                to_mp3_vbr(self.source, temp_filename, 2)
            elif self.quality == Q.AAC_V5:
                temp_filename = temp_filename.replace(".mp3", ".m4a")
                to_aac(self.source, temp_filename)

            # convert back to flac
            to_flac(temp_filename, self.filename)
            # remove temp files
            os.remove(temp_filename)
            return True

    def generate_samples(self, full_specs_dir, specs_dir):
        """
        Generates the spectrogram and slices it into 128x128 square images.
        :param full_specs_dir: directory for the full spectrogram
        :param specs_dir: directory for the spectrogram slices
        :return: number of slices generated
        """
        num_slices = 0
        s = Spectrogram(self.filename, good_encode=self.good_encode)
        if s.generate(full_specs_dir):
            num_slices += s.slice(specs_dir, self.set)
        return num_slices


class SourceFile:
    """
    A SourceFile is linked to an original flac file in the source directory.
    The original file will be untouched, but other audio files will be generated from it in order to create spectrograms.
    """
    def __init__(self, filename):
        self.filename = filename
        self.audio_files = []
        self.hash = self.md5()
        self.set = ""
        self.category = Looks.FUZZY

    def md5(self):
        """
        For identification purposes and to avoid collisions, the associated AudioFiles and spectrograms will be named
        after the SourceFile's md5 hash, calculated over its first 500kb.
        :return:
        """
        hash_md5 = hashlib.md5()
        with open(self.filename, "rb") as f:
            # md5 over the first 500kb
            hash_md5.update(f.read(500 * 1024))
        return hash_md5.hexdigest()

    def assign_set(self, val_pct, test_pct):
        """
        Assign a subset (training, validation, test) to the source file. All associated files that will be created
        (transcodes, copies, spectrograms) will belong to this subset.
        This ensures close spectrogram slices (ex: from the 320CBR mp3 & 256CBR mp3) end up in different subsets and
        therefore weaken the training abilities of the data set.
        :param val_pct: % of the data set dedicated to validation
        :param test_pct: % of the data set dedicated to tests
        """
        np.random.seed()
        dice_throw = np.random.rand(1)
        if dice_throw < test_pct:
            self.set = Subset.TEST
        elif test_pct <= dice_throw < (test_pct + val_pct):
            self.set = Subset.VALIDATION
        else:
            self.set = Subset.TRAINING
        np.random.seed()
        another_dice_throw = np.random.rand(1)
        if another_dice_throw < 0.5:
            self.category = Looks.GOOD
        else:
            self.category = Looks.BAD

    def transcode(self, audio_dir):
        """
        Generates AudioFiles for every valid quality Q, and transcodes what must be.
        :return: True if at least one audio file was created
        """
        if self.category == Looks.GOOD:
            self.audio_files.append(AudioFile(self.filename, self.hash, self.set, Q.FLAC, Looks.GOOD, audio_dir))
        else:
            for quality in Q:
                if quality == Q.FLAC:
                    continue
                self.audio_files.append(AudioFile(self.filename, self.hash, self.set, quality, Looks.BAD, audio_dir))

        has_anything_been_transcoded = False
        for f in self.audio_files:
            if f.transcode():
                has_anything_been_transcoded = True
        return has_anything_been_transcoded

    def generate_samples(self, full_specs_dir, specs_dir):
        num_samples = 0
        for f in self.audio_files:
            num_samples += f.generate_samples(full_specs_dir, specs_dir)
        return num_samples

    def prepare(self, audio_dir, full_specs_dir, specs_dir, val_pct, test_pct):
        self.assign_set(val_pct, test_pct)
        if self.transcode(audio_dir):
            return self.generate_samples(full_specs_dir, specs_dir)
        return 0

    def predict(self, trained_model, image_size, batch_size):
        if not has_right_sampling_rate(self.filename):
            print(Fore.RED + Style.BRIGHT + "File " + self.filename + " has sampling rate other than 44.1kHz, ignoring.")
            return
        with tempfile.TemporaryDirectory() as tmpdirname:
            # the real target here is the source file itself
            self.audio_files.append(AudioFile(self.filename, "aaa", Subset.PREDICT, Q.FLAC, Looks.FUZZY,
                                              os.path.join(tmpdirname, "audio_data"), predict=True))
            # generate spectrogram slices
            self.generate_samples(tmpdirname, os.path.join(tmpdirname, "input_data"))
            # load images in a generator
            predict_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
            predict_generator = predict_datagen.flow_from_directory(
                tmpdirname,
                target_size=(image_size, image_size),
                batch_size=batch_size,
                shuffle=False,
                class_mode=None,
                color_mode='grayscale',
            )
            # predict for all slices
            predictions = trained_model.predict_generator(predict_generator, use_multiprocessing=True)
            # returns a numpy array of predictions the file is a good encode (0 == bad transcodes, 1 == good encodes)
            # take the average of all probabilities
            probability = 100 - 100 * np.average(predictions)
            # show info
            if probability > 25:
                color = Fore.RED + Style.BRIGHT
            else:
                color = Fore.GREEN + Style.BRIGHT
            print(color + "There is a %.2f%% probability this audio file is a bad transcode." % probability)


def analyze(source_files, models, image_size, batch_size):
    for model in models:
        # load model
        print(Fore.BLUE + "Predicting using model " + model + "...")
        trained_model = load_model(model)
        for f in source_files:
            print(Fore.BLUE + Style.BRIGHT + "Analyzing " + f.filename + "...")
            f.predict(trained_model, image_size, batch_size)
